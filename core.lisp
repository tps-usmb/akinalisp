;======
;UTILS
;==========================================================================
(defun put (sym prop val)
  (setf (get sym prop) val))

;TODO faire marcher puis remplacer remove par rmelt
(defun rmelt (e l)
  (mapcar (lambda (e2) (cond ((not (eq e e2)) e2))) l))

(defun rmdup (l)
  (cond ((null l) l)
        ((member (car l) (cdr l)) (rmdup (cdr l)))
        (t (cons (car l) (rmdup (cdr l))))))

(defun si (cnd alors sinon)
    (cond ((eval cnd) (eval alors))
          (t (eval sinon))))

(defun test (f)
  (print f)(write-line ":")(write (eval f))(print '--- ))

;=====
;CORE
;==========================================================================
(setq node_list '())
(setq relation_list '())

(defun defnode (n)
   (cond ((member n node_list) (print 'existe_deja) nil)
         (t (setq node_list (cons n node_list)) n)))

(defun defnodelist (l)
   (cond ((listp l) (mapcar 'defnode l))
         (t (defnode l))))

(defun putrelation (a r b)
  ; si deja lie -> list
  (cond ((member b (get a r)) (get a r))

        ; si b et a sont des noeuds
        ((and (member b node_list) (member a node_list))
         ; update la liste des relations
         (setq relation_list (rmdup (cons r relation_list)))
         (put a 'relation_list (rmdup (cons r (get a 'relation_list))))
         ; update les relations r
         (put a r (cons b (get a r))))

        ; sinon
        (t (get a r))))

(defun putrelations (a r l)
  (mapcar (lambda (b) (putrelation a r b)) l))

; TODO optimiser (factoriser rmdup)
(defun getvalue (n r) ; noeuds -> relations -> noeuds
  (rmdup (append (get n r) (mapcan (lambda (n2) (getvalue n2 r)) (get n 'isa)))))

;=================
;AUTRES FONCTIONS
;==========================================================================
(defun nodelist () node_list)
(defun relationlist () relation_list)
(defun nodep (n) (cond ((member n node_list) t) (t nil)))
(defun relationp (r) (cond ((member r relation_list) t) (t nil)))
(defun linkednodes (n) (mapcan (lambda (r) (get n r)) (get n 'relation_list)))
(defun removevalue (a r b)
  (put a r (remove b (get a r)))
  (cond ((null (get a r)) (remove r (get a 'relation_list))))
  (get a r))

;=======
;MOTEUR
;==========================================================================
(setq CA '(<= (length possibility_list) 1))

(defun newrule (rule condition action)
    (put rule 'condition condition)
    (put rule 'action action)
    ; si r
    (cond ((null (member rule rule_list)) (setq rule_list (cons rule rule_list)))
	        (t nil)))

(defun ruleapplicable(rule)
  (cond ((eval (get rule 'condition)) (print rule) t)
          (t  nil)))

(defun applyrule(rule)
  (eval (get rule 'action)))

(defun keeprule(rule)
  (cond ((member rule REMAINING_RULES) () nil )
        (t (setq REMAINING_RULES (cons rule REMAINING_RULES)) rule)) )

(defun run ()
  (cond ((eval CA) (print 'a))
        (t (mapc (lambda (r) (if (ruleapplicable r) (applyrule r))) rule_list) (print 'c)))
  (print possibility_list))
