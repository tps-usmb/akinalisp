(load "constants.lisp")
(load "engine.lisp")
;=====
;TEST
;==========================================================================
(test '(defnode 'test))
(test '(defnodelist '(jean human pote marteau)))

(test '(putrelation 'jean 'isa 'human))
(test '(putrelation 'jean 'isa 'pote))
(test '(putrelation 'jean 'isa 'aoeu))

(putrelation 'jean 'possede 'marteau)
(putrelation 'human 'possede 'marteau)
(putrelation 'human 'possede 'test)

;(print (get 'jean 'isa))
;(print (get 'jean 'possede))
(test '(getvalue 'jean 'possede))
;(print (getvalue 'jean 'isa))
;
;(putrelation 'jean 'isa 'human)
;(putrelation 'jean 'isa 'pote)
;(print (getvalue 'jean 'isa))

(test '(nodelist))
(test '(relationlist))
(test '(nodep 'jean))
(test '(nodep 'isa))
(test '(relationp 'jean))
(test '(relationp 'isa))
(test '(linknodes 'human))
(test '(removevalue 'human 'possede 'marteau))
(test '(linknodes 'human))
(test '(removevalue 'human 'possede 'test))
(test '(linknodes 'human))
