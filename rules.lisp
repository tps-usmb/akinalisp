(setq rule_list '())
(setq REMAINING_RULES '())

(defun eliminate (prop)
  (mapcar (lambda (n) 
                            (cond ((member prop (get n 'isa)) (print prop) n)
                                (t nil))) possibility_list))

; CONNAISSANCES
(setq possibility_list '(Rhyolite Trachyte Andesite Basalte Granite Granodiorite Gabbro Peridotite))
(defnodelist '(MagmatiqueVolcanique Mantellique MagmatiquePlutonique Quartz QuartzPresent QuartzAbsent FeldspathPotassique FeldspathPotassiquePresent FeldspathPotassiqueAbsent  PlagioclasesAbondants PlagioclasesMajoritairesAuFeldspath PlagioclasesMinoritairesAuFeldspath  AmphibolesEtBiotiteFrequentes FerroMagnesiensAbondants  Rhyolite Trachyte Andesite Basalte Granite Granodiorite Gabbro Peridotite))

(putrelation 'Rhyolite 'isa 'MagmatiqueVolcanique)
(putrelation 'Rhyolite 'has 'QuartzPresent)

(putrelation 'Trachyte 'isa 'MagmatiqueVolcanique)
(putrelation 'Trachyte 'hasno 'Quartz)
(putrelation 'Trachyte 'has 'FeldspathPotassique)

(putrelation 'Andesite 'isa 'MagmatiqueVolcanique)
(putrelation 'Andesite 'hasno 'Quartz)
(putrelation 'Andesite 'hasno 'FeldspathPotassique)
(putrelation 'Andesite 'has 'AmphibolesEtBiotiteFrequentes)

(putrelation 'Basalte 'isa 'MagmatiqueVolcanique)
(putrelation 'Basalte 'hasno 'Quartz)
(putrelation 'Basalte 'hasno 'FeldspathPotassique)
(putrelation 'Basalte 'has 'FerroMagnesiensAbondants)

(putrelation 'Granite 'isa 'MagmatiquePlutonique)
(putrelation 'Granite 'has 'Quartz)
(putrelation 'Granite 'has 'FeldspathPotassique)

(putrelation 'Granodiorite 'isa 'MagmatiquePlutonique)
(putrelation 'Granodiorite 'has 'Quartz)
(putrelation 'Granodiorite 'has 'PlagioclasesAbondants)

(putrelation 'Gabbro 'isa 'MagmatiquePlutonique)
(putrelation 'Gabbro 'hasno 'Quartz)
(putrelation 'Gabbro 'has 'FerroMagnesiensAbondants)

(putrelation 'Gabbro 'isa 'MagmatiquePlutonique)
(putrelation 'Gabbro 'hasno 'Quartz)
(putrelation 'Gabbro 'hasno 'FeldspathPotassique)

; RULES
(newrule 'R_isa_MagmatiqueVolcanique
         '(null (member 'MagmatiqueVolcanique (get 'roche 'isa)))
         '(setq possibility_list (eliminate 'MagmatiqueVolcanique)))


(newrule 'R_posses_Quartz
         '(null (member 'QuartzPresent (get 'roche 'posses)))
         '(setq possibility_list (eliminate 'QuartzPresent)))
